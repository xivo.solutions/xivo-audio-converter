# -*- coding: utf-8 -*-
import logging
import itertools
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler
from subprocess import check_output, STDOUT, CalledProcessError
from os.path import splitext
from os import remove

logger = logging.getLogger(__name__)


class AudioHandler(PatternMatchingEventHandler):
    patterns = ["*.wav"]

    def __init__(self, config):
        PatternMatchingEventHandler.__init__(self)
        self.src_dir = config['src_dir']
        self.dst_dir = config['dst_dir']
        self.compressor = config['compressor']
        self.compressor_params = list(config['compressor_params'].split(' '))
        self.dst_file_extension = config['dst_file_extension']

    def convert_file(self, file):
        logger.info('Converting  %s using: %s with parameters: %s' % (file, self.compressor, self.compressor_params))
        try:
            check_output(list(itertools.chain.from_iterable([[self.compressor],
                          self.compressor_params,
                          [file],
                          ['%s.%s' % (splitext(file)[0].replace(self.src_dir, self.dst_dir), self.dst_file_extension)]])),
                         stderr=STDOUT)
        except CalledProcessError as e:
            logger.error('Error converting %s' % file)
            logger.error('Captured output : %s' % e.output)
        else:
            remove(file)

    def on_created(self, event):
        self.convert_file(event.src_path)

    def on_moved(self, event):
        self.convert_file(event.dest_path)


class Converter:

    def __init__(self, config):
        self.config = config

    def run(self):
        observer = Observer()
        observer.schedule(AudioHandler(self.config), path=self.config['src_dir'], recursive=True)
        logger.info('Starting the observer')
        observer.start()
        try:
            while observer.is_alive():
                observer.join(1)
        finally:
            logger.info('Stopping the observer')
            observer.stop()
        observer.join()
